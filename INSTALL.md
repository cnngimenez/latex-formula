# Remember

First try the `install` script, because it will try to do all these steps. If something goes wrong, well, you have no other choice... :-P

# Requirements

You need the following programs:

* The TexLive project with the PDFLaTeX compiler.
* `convert` utility from [ImageMagick](http://www.imagemagick.org/script/convert.php).

## Fedora Based 
If you have `yum` available in your terminal you can use this line to install both packages:

	sudo yum install ImageMagick texlive -y
	
# Downloading

There are two ways for downloading this package. 

## Using GIT
If you're a git fan, or you want to update from time to time (despite I won't update this project so often!) you can use git as follows:

    git clone https://github.com/cnngimenez/latex-formula.git

## As a ZIP
Github allows to download a ZIP file with all the sources. You can do this through the follwing [link](https://github.com/cnngimenez/latex-formula/archive/master.zip) or using a command line tool.

Here are some examples for downloading using the terminal:

* Using WGet: `wget https://github.com/cnngimenez/latex-formula/archive/master.zip`
* Using CURL: `curl https://github.com/cnngimenez/latex-formula/archive/master.zip`

# Installing in Emacs
Unzip if necessary and copy everything into ~/emacsstuff/formula diretory, create them if necessary or you'll have to configure all the paths using the "configure" feature.

Insert this code into the ~/.emacs personal init file:

	(setq load-path (cons "~/emacsstuff/formula/formula.el" load-path))
	(require 'formula)

Ensure that this code is reachable and is at the last part of the file. Try to don't change nor delete all the code that already is there!

# License 

	INSTALL.md
	Copyright (C) 2013  Giménez, Christian N.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Viernes 23 De Agosto Del 2013


