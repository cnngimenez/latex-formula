latex-formula
=============

Have you ever tried to submit a complex formula or a latex code into a social net?

What about sharing your work in Diaspora or in Pumpio?


This code brings you a way for compiling easy and fast some LaTeX code into PNG image format.


![lion-logo](http://latex-project.org/lib/img/lion.png) : See the [LaTeX Project Page](http://www.latex-project.org/).

![emacswiki](http://emacswiki.org/ew_logo.png) : See the [EmacsWiki Page](http://www.emacswiki.org/).


# Requirements and Installation

* First, try the `install` script.
* See [INSTALL file](https://github.com/cnngimenez/latex-formula/blob/master/INSTALL.md) for a complete installation if the latter doesn't work (of if you're curious!).

# Using 

Take a look at the [USING file](https://github.com/cnngimenez/latex-formula/blob/master/USING.md) !


----

*Happy Coding!!!*

----

# License

    README.md
    Copyright (C) 2013  Giménez, Christian N.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Jueves 15 De Agosto Del 2013    


