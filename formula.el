;;; formula.el --- Compile simple LaTeX snippets to see its results -*- lexical-binding: t; -*-

;; Copyright 2013 Gimenez, Christian
;;
;; Author: Gimenez, Christian
;; Version: 0.1.0
;; Keywords: writing, convenience
;; URL: https://bitbucket.org/cnngimenez/latex-formula
;; Package-Requires: ((emacs "24.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:
;;
;; Compiles a snippet code in LaTeX and show the results.  Basically, it
;; completes a template with the user-provided snippet, compiles it, converts it
;; to PNG image, and shows in a lateral buffer.
;;
;; The template is a LaTeX file with the string "((>> <<))" marking where the
;; snippet should be placed.  There are templates available by default at this
;; package.
;;
;; Its usage sequence is as follows:
;; 1) Create a new formula buffer with \\[formula-new].
;; 2) Write your LaTeX/Tikz code.  No need for \\documentclass or preamble.
;; 3) Call \\[formula-compile-current-buffer].  The results will be shown at a
;;    side buffer.
;;
;; See `formula-mode' for more information.
;;

;;; Code:

					; ____________________
					;
					; Customizations
					; ____________________

(defgroup formula nil "Making formulas using a piece of LaTeX."
  :group 'applications
  :version "23.0"
  :tag "Formulae in LaTeX")

(defcustom formula-temporal-dir "~/tmp"
  "This is the temporal directory using for compiling the formula.
Temporal directory to compile TeX and to create the PNG image."
  :group 'formula
  :type 'directory
  :tag "Temporal Directory")

(defcustom formula-template-dir "~/emacsstuff/formula/templates"
  "Where do I find the tex templates?"
  :group 'formula
  :type 'directory
  :tag "Template Directory")

(defcustom formula-current-template "math.tex"
  "Which is my default template I should use?
If not specified I use this template.

Use \\[formula-change-template] for changing this variable temporary."
  :group 'formula
  :type 'string
  :tag "Default Template File")

					; ____________________


(defconst formula-buffer-name "LaTeX Formula")

(defconst formula-output-buffer "LaTeX Formula Output")
(defconst formula-error-buffer "LaTeX Formula Error")

(defvar formula-is-formula-working nil
  "Is formula working in this buffer?
This is a buffer setted variable.  Globaly should has the nil value.")

(defvar formula-available-templates nil
  "List of available templates, this is created using `formula-get-templates'.")

(defvar formula-image-buffer nil
  "The image buffer where the formula is displayed.")

(defvar formula-current-buffer nil
  "The current formula buffer.")

(defun formula-get-templates ()
  "Create the list of available templates.
Get all the TeX filenames in the template directory `formula-template-dir' and
create a list of available templates, store the results at
`formula-available-templates'."
  (setq formula-available-templates (directory-files formula-template-dir nil ".*\.tex$")) )
  

(defun formula-find-templates ()
  "Use `find-file' to access the template directory."
  (interactive)
  (find-file formula-template-dir))

(defun formula-new ()
  "Open a buffer for writing a formula and compiling it returning the code."
  (interactive)
  (get-buffer-create formula-output-buffer)
  (get-buffer-create formula-error-buffer)
  (with-current-buffer (find-file-noselect (formula-tmp-file "formula.tex"))
    (switch-to-buffer-other-window (current-buffer))
    (setq formula-buffer-name (buffer-name (current-buffer)))
    (setq formula-current-buffer (current-buffer))
    
    (fit-window-to-buffer (selected-window) 10 10)

    (latex-mode)
    (formula-mode 1)

    (message "Press C-c C-c to compile and see the formula")
    ;;(define-key latex-mode-map "\C-c\C-c" 'formula-compile-current-buffer)
    ))

(defun formula-current-template ()
  "Show to the user the current template file."
  (interactive)
  (message (concat "Current template is: " formula-current-template)) )

(defun formula-change-template (template-file)
  "Change the current template.
TEMPLATE-FILE is the filename of the template to change."
  (interactive (let ((string (progn
			       (formula-get-templates)
			       (completing-read "Template file?" formula-available-templates))) )
		 (list string)))
  (setq formula-current-template template-file) )

;; ;; No need for advising with the `formula-mode' minor mode...
;;
;; (defun formula-compile-if-formula (orig-fun &rest args)
;;   "Verify before executing `tex-compile' if this is a formula buffer.
;; Execute `formula-compile-current-buffer' and exits, if this is a formula
;; buffer, instead of executing the `tex-compile' as usual.

;; This is an around advising for `tex-compile'.
;; ORIG-FUN is the advised function (`tex-compile' supposedly).  ARGS the
;; arguments used."
;;   (if formula-is-formula-working
;;       (progn
;; 	(formula-compile-current-buffer))
;;     (apply orig-fun args)))
  
(defun formula-compile-current-buffer ()
  "Prepare the template, compiles and show the results.

Use the LaTeX source from current buffer and the template to create a complete
LaTeX file at a temporary location.  Compile it, generate a PNG file, and show
it."
  (interactive)
  (setq formula-current-buffer (current-buffer))
  (save-current-buffer
    (let ((str (buffer-string)))
      (with-current-buffer (find-file-noselect (formula-template-path))
        (goto-char (point-min))
        (when (search-forward "((>> <<))")
          (replace-match str nil t))
        (write-file (formula-tmp-file "salida.tex"))
        (kill-buffer)
        (save-current-buffer
          (formula-compile))))))

(defun formula--show-image-buffer ()
  "Show or reset the image buffer."
  (unless (null (buffer-name formula-image-buffer))
    (kill-buffer formula-image-buffer))
  (with-current-buffer
      (find-file-other-window (formula-tmp-file "salida.png"))
    (setq formula-image-buffer (current-buffer))
    (setq buffer-read-only t)))

(defun formula--show-error-buffer ()
  "Show the error buffer to display compilation errors."
  (switch-to-buffer-other-window formula-error-buffer))

(defun formula--show-formula-buffer ()
  "Show the formula buffer on one panel."
  (switch-to-buffer formula-current-buffer))

(defvar formula-window-config nil
  "The frame layout used before compiling.")

(defun formula-show ()
  "Display the formula buffer and the results at its side."
  (interactive)
  (if (null formula-window-config)
      (progn
        (switch-to-buffer formula-current-buffer nil t)
        (formula--show-image-buffer)
        (select-window (get-buffer-window formula-current-buffer))
        (setq formula-window-config (current-window-configuration)))
    (set-window-configuration formula-window-config)))

(defun formula--clean-compilation ()
  "Clean the last compiled files."
  (delete-file (formula-tmp-file "salida.png")))


(defun formula-compile ()
  "Compile the temporal TeX file."
  ;; (formula--clean-compilation)
  (shell-command (formula-1command)
                 formula-output-buffer formula-error-buffer)
  (if (file-exists-p (formula-tmp-file "salida.pdf"))
      (shell-command (formula-2command)
                     formula-output-buffer formula-error-buffer)
    (progn
      (message "Errors when compiling.")
      (formula--show-error-buffer)))

  (if (file-exists-p (formula-tmp-file "salida.png"))
      (progn
        (formula--show-image-buffer)
        (message (concat
                  "Output is at "
                  (formula-tmp-file "salida.png")))
        (formula-show))
    (progn
      (message "Errors when compiling.")
      (formula--show-error-buffer))))

(defun formula-1command ()
  "First command used for converting from TeX to PDF."
  (format "cd %s; pdflatex %s"
	  formula-temporal-dir
	  (formula-tmp-file "salida.tex")))

(defun formula-2command ()
  "Second command used for converting from PDF to PNG."
  (format "cd %s; convert -trim -border 5 \"salida.pdf[0]\" salida.png"
	  formula-temporal-dir))

(defun formula-last-char-div (s)
  "Return t if the last char is \"/\".
S is the string to check for the last char."
  (if (> (length s) 0)
      (equal (substring s (- (length s) 1)) "/")
    nil))

(defun formula-tmp-file (filename)
  "Return the entire path according to the customization `formula-temporal-dir'.
FILENAME is the string with the filename to be used as suffix.  The temporal
dir will be append at the begining of FILENAME."
  (let ((sep (if (formula-last-char-div formula-temporal-dir)
		 ""
	       "/")))
    (concat formula-temporal-dir sep filename)))

(defun formula-template-path (&optional filename)
  "Return the entire path for the given template.
If FILENAME is not specified or nil, then return the default template path."
  (let ((sep (if (formula-last-char-div formula-temporal-dir)
		 ""
	       "/")))
    (concat formula-template-dir sep (if (not filename)
					 formula-current-template
				       filename))))

(define-minor-mode formula-mode
  "Toggle formula-mode.
Create simple LaTeX formulas and display them with just
 \\=C\\=-c  \\=C\\=-c (by default see keymaps below).
Use a document template from the path setted at `formula-template-dir'.

Interactive commands:
- `formula-new' : Create a new formula buffer.
- `formula-compile-current-buffer' or \\[formula-compile-current-buffer]:
   Compile current buffer and show the results.
- `formula-change-template': Change the current template.
- `formula-current-template' : Display the current template file.

Customizations are at the \"formula\" group.

Special commands and keymaps:
\\{formula-mode-map}"
  :lighter "Formula"
  :keymap '(
            ("\C-c\C-c" . formula-compile-current-buffer))
  :group 'formula

  (set (make-local-variable 'formula-is-formula-working) t))
  ;; (advice-add 'tex-compile :around #'formula-compile-if-formula))

(provide 'formula)
;;; formula.el ends here
