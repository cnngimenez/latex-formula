;;; formula.el --- 

;; Copyright 2013 Giménez, Christian
;;
;; Author: Giménez, Christian
;; Version: $Id: formula.el,v 0.0 2013/08/13 21:48:50 Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'formula)

;;; Code:

(provide 'formula)

					; ____________________
					;
					; Customizations
					; ____________________

(defgroup formula nil "Making formulas using a piece of LaTeX."
  :group 'applications
  :version "23.0"
  :tag "Formulae in LaTeX"
  )

(defcustom formula-temporal-dir "~/tmp"
  "This is the temporal directory using for compiling the formula and creating the PNG image."
  :group 'formula
  :type 'directory
  :tag "Temporal Directory"
  )

(defcustom formula-template-dir "~/emacsstuff/formula/templates"
  "Where do I find the tex templates?"
  :group 'formula
  :type 'directory
  :tag "Template Directory"
  )

(defcustom formula-current-template "math.tex"
  "Which is my default template I should use?
If not specified I use this template.

Use M-x `formula-change-template' for changing this variable temporary."
  :group 'formula
  :type 'string
  :tag "Default Template File"
  )

					; ____________________


(defconst formula-buffer-name "LaTeX Formula")

(defconst formula-output-buffer "LaTeX Formula Output")
(defconst formula-error-buffer "LaTeX Formula Error")

(defvar formula-is-formula-working nil
  "Is formula working in this buffer? 
This is a buffer setted variable. Globaly should has the nil value."
  )

(defvar formula-available-templates nil
  "List of available templates, this is created using `formula-get-templates'.")

(defun formula-get-templates ()
  "Create the list of available templates (`formula-available-templates') getting all the TeX filenames in the template directory `formula-template-dir'."
  (setq formula-available-templates (directory-files formula-template-dir nil ".*\.tex$"))  
  )
  

(defun formula-new ()
  "Open a buffer for writing a formula and compiling it returning the code."
  (interactive)
  (get-buffer-create formula-output-buffer)
  (get-buffer-create formula-error-buffer)
  (with-current-buffer (find-file-noselect (formula-tmp-file "formula.tex"))
    (switch-to-buffer-other-window (current-buffer))
    (fit-window-to-buffer (selected-window) 10 10)

    (latex-mode)

    (set (make-local-variable 'formula-is-formula-working) t)
    
    (message "Press C-c C-c to compile and see the formula")    
    ;;(define-key latex-mode-map "\C-c\C-c" 'formula-compile-current-buffer)    
    )
  )

(defun formula-current-template ()
  "Show to the user the current template file."
  (interactive)
  (message (concat "Current template is: " formula-current-template))
  )

(defun formula-change-template (template-file)
  (interactive (let ((string (progn
			       (formula-get-templates)
			       (completing-read "Template file?" formula-available-templates)))
		     )
		 (list string)))
  (setq formula-current-template template-file)
  )

(defadvice tex-compile (around tex-compile-verify-is-formula (dir cmd))
  "Verify before executing `tex-compile' if this is a formula buffer. If it is, execute `formula-compile-current-buffer' and exits instead of executing the tex as usual."
  (if formula-is-formula-working
      (progn
	(formula-compile-current-buffer)
	)
    ad-do-it
    )
  )

(eval-and-compile 
  (ad-activate 'tex-compile)
  )

(defun formula-compile-current-buffer ()
  "Prepare the template, compiles and show the results taking the LaTeX source from current buffer and using the template."
  (interactive)
  (let ((str (buffer-string)))
    (with-current-buffer (find-file-noselect (formula-template-path))
      (goto-char (point-min))
      (when (search-forward "((>> <<))")
	(replace-match str nil t)
	)
      (write-file (formula-tmp-file "salida.tex"))
      (kill-buffer)
      (formula-compile)
      )
    )
  )

(defun formula-compile ()
  "Compile the temporal TeX file"
  (shell-command (formula-1command)
		 formula-output-buffer formula-error-buffer)
  (shell-command (formula-2command)
		 formula-output-buffer formula-error-buffer)  
  (find-file (formula-tmp-file "salida.png"))
  (message (concat 
	    "Output is at "
	    (formula-tmp-file "salida.png"))
	   )
  )

(defun formula-1command ()
  "First command used for converting from TeX to PDF."
  (format "cd %s; pdflatex %s" 
	  formula-temporal-dir
	  (formula-tmp-file "salida.tex")
	  )
  )

(defun formula-2command ()
  "Second command used for converting from PDF to PNG."
  (format "cd %s; convert -trim -border 5 \"salida.pdf[0]\" salida.png"
	  formula-temporal-dir	  
	  )
  )

(defun formula-last-char-div (s)
  "Return t if the last char is \"/\"."
  (if (> (length s) 0)
      (equal (substring s (- (length s) 1)) "/")
    nil)
  )

(defun formula-tmp-file (filename)
  "Return the entire path according to the customization `formula-temporal-dir'."
  (let ((sep (if (formula-last-char-div formula-temporal-dir)
		 ""
	       "/")))
    (concat formula-temporal-dir sep filename)
    )
  )

(defun formula-template-path (&optional filename)
  "Return the entire path for the given template. If filename is not specified or nil, then I return the default template path."

  (let ((sep (if (formula-last-char-div formula-temporal-dir)
		 ""
	       "/")))
    (concat formula-template-dir sep (if (not filename)
					 formula-current-template
				       filename))
    )
  )
;;; formula.el ends here
