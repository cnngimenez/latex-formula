

# Considerations

* Every time you read `M-x` we refer to Alt and X keys.
* `C-c C-c` Is Control and C keys pressed twice.

Take a look at the [EmacsWiki article](http://www.emacswiki.org/emacs/EmacsKeyNotation) about this if you're too new! 

# How to use this?

After you have [installed](https://github.com/cnngimenez/latex-formula/blob/master/INSTALL.md) all, you can write LaTeX snippets code and compile it doing this:


1. Type `M-x formula-new`.
2. Write your snippet... for example: `Right! $E = mc^2$ ... Thanks Einstein!`
3. Press `C-c C-c`, it will start compiling and doing lot of things...
4. When it finish will appear a new buffer with an image inserted.
5. Here you have two choices: 
   - You can press `C-x C-w` for writing the file with another name and path or...
   - Just go to the temporary directory and find the image (look! look! at that message in the minibuffer telling you where to find it!).

# Templates?

They are LaTeX files located in the template directory (press `C-h v formula-template-dir` if you don't know where is that). This will be used for compiling with pdflatex.



*Note:* If you used the install script, all the template files are at ~/emacsstuff/formula/tempates directory.


## Creating One
The only thing you should know for creating a template is that you have to put `((>> <<))` where the snippet will be when pressing `C-c C-c`.

In other words, the first `((>> <<))` characters founded will be replaced with the snippet when compiling.

You can add all the packages you want available and all what you like.


If you want to see an example, take a look at math.tex template! Come one! All this Is [Software Libre](http://fsf.org) !


*Remember:* 

* The template is a LaTeX file ready for compiling.
* The template should create only one page! if not it will not appear!


## Using a Template
For changing the default template you can type `M-x formula-change-template` and write the tempate file. You can use the autocomplete feature with the "tab" key.

All .tex files in the template directory will be available.

## Which Template I'm Using?

Easy! Just type `M-x formula-current-template` and Emacs will tell you wich template filename is the current one. 



----

*Happy coding!!!*

----




# License


    USING.md
    Copyright (C) 2013  Giménez, Christian N.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Viernes 23 De Agosto Del 2013

